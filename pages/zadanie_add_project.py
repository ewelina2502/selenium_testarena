from selenium.webdriver import Chrome
import string
import random


class TestArenaProjectPage:

    def __init__(self, browser: Chrome):
        self.browser = browser
        self.random_text = random_text
        self.prefix_name = prefix_name

    def add_project(self):
        button_project = self.browser.find_element_by_css_selector("li.button_link_li")
        button_project.click()

        name_of_project = self.browser.find_element_by_id("name")
        assert name_of_project.is_displayed()

    def add_name_of_project(self):
        add_name = self.browser.find_element_by_id("name")
        add_name.send_keys(self.random_text)

    def send_prefix(self):
        prefix = self.browser.find_element_by_id("prefix")
        prefix.send_keys(self.prefix_name)

    def save_project(self):
        button_save = self.browser.find_element_by_id("save")
        button_save.click()

    def verification(self):
        button_close = self.browser.find_element_by_class_name("j_close_button")
        assert button_close.is_displayed()

    def verification_name(self):
        title_project = self.browser.find_element_by_id("text.content_label_title")
        assert title_project.is_displayed()

    def add_name_for_search(self):
        add_name_search = self.browser.find_element_by_id("search")
        add_name_search.send_keys(self.random_text)


random_text = ''.join(random.choices(string.ascii_uppercase + string.digits, k=10))
prefix_name = ''.join(random.choices(string.ascii_uppercase + string.digits, k=6))
