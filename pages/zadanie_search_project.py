from selenium.webdriver import Chrome


class SearchProject:

    def __init__(self, browser: Chrome):
        self.browser = browser

    def project_menu(self):
        button_projekty = self.browser.find_element_by_class_name("activeMenu")
        button_projekty.click()

    def search_project(self):
        input_search = self.browser.find_element_by_id("search")
        input_search.click()

    def search_button_click(self):
        search_button = self.browser.find_element_by_id("j_searchButton")
        search_button.click()

    def veryfication_search(self):
        project_ok = self.browser.find_element_by_class_name("t_number")
        assert project_ok.is_displayed()
