from selenium.webdriver import Chrome


class TestArenaLoginPage:
    def __init__(self, browser: Chrome):
        self.browser = browser

    def login(self, administrator_email, administrator_password):
        login_bar = self.browser.find_element_by_id("email")
        password_bar = self.browser.find_element_by_id("password")
        button_login = self.browser.find_element_by_id("login")
        login_bar.send_keys(administrator_email)
        password_bar.send_keys(administrator_password)
        button_login.click()



