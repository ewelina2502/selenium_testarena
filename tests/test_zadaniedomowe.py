import pytest
from selenium.webdriver import Chrome
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions
from pages.zadanie_add_project import TestArenaProjectPage
from pages.zadanie_login_page import TestArenaLoginPage
from pages.zadanie_search_project import SearchProject
from pages.zadanie_test_arena_home_page import TestArenaHomePage


@pytest.fixture()
def browser():
    browser = Chrome(executable_path=ChromeDriverManager().install())
    browser.maximize_window()
    browser.get("http://demo.testarena.pl/zaloguj")
    yield browser
    browser.quit()


def test_testarena(browser):
    login_page = TestArenaLoginPage(browser)
    login_page.login('administrator@testarena.pl', 'sumXQQ72$L')

    arena_home = TestArenaHomePage(browser)
    arena_home.verify_succesfull_login()
    arena_home.open_admin_panel()

    wait = WebDriverWait(browser, 10)
    button_add = (By.CSS_SELECTOR, "li.button_link_li")
    wait.until(expected_conditions.visibility_of_element_located(button_add))

    arena_home = TestArenaProjectPage(browser)
    arena_home.add_project()

    add_name = TestArenaProjectPage(browser)
    add_name.add_name_of_project()

    prefix = TestArenaProjectPage(browser)
    prefix.send_prefix()

    button_save = TestArenaProjectPage(browser)
    button_save.save_project()

    button_close = TestArenaProjectPage(browser)
    button_close.verification()


def test_testarena_search_project(browser):
    login_page = TestArenaLoginPage(browser)
    login_page.login('administrator@testarena.pl', 'sumXQQ72$L')

    arena_home = TestArenaHomePage(browser)
    arena_home.verify_succesfull_login()
    arena_home.open_admin_panel()

    input_search = SearchProject(browser)
    input_search.search_project()

    add_name_search = TestArenaProjectPage(browser)
    add_name_search.add_name_for_search()

    search_button = SearchProject(browser)
    search_button.search_button_click()

    project_ok = SearchProject(browser)
    project_ok.veryfication_search()

